
This module falls under the category of "let's improve the User experience".
Since this module tries to improve the UI, the button text is configurable.

It extends the user inteface for creating content by providing a pulldown
block of content types and a button for quickly creating new content,
the block is OG aware and adds the group id to the links when on an OG page

It was primarily developed because the UI for creating content was clumsy,
especially when comparing the difference between creating content when in
and not-in an OG page.

But while this block is OG aware, it does not rely on OG.  And it has
definite value to non-OG sites by providing a simple create content block.


Author
------
Doug Green
douggreen@douggreenconsulting.com
