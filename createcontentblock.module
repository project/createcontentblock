<?php

/**
 * @file
 * Create Content Block (or node/add Block)
 */

/**
 * Implementation of hook_menu().
 * used only when adding content types for OG
 */
function createcontentblock_menu($may_cache) {
  $items = array();
  if ($may_cache) {
    $items[] = array(
      'path' => 'node/add-og',
      'title' => t('Create content'),
      'callback' => 'createcontentblock_node_add_page',
      'access' => user_access('access content'),
      'type' => MENU_CALLBACK,
    );
  }
  return $items;
}

/**
 * Implementation of hook_block().
 */
function createcontentblock_block($op = 'list', $delta = 0, $edit = array()) {
  switch ($op) {
    case 'list':
      $blocks['main']['info'] = t('Create Content / Node Add');
      return $blocks;

    case 'configure':
      $form['button_label'] = array(
        '#type' => 'textfield',
        '#title' => t('Button Label'),
        '#default_value' => variable_get('createcontentblock_button_label', t('Create Content')),
        '#description' => t('You can change the button label'),
      );
      return $form;

    case 'save':
      variable_set('createcontentblock_button_label', $edit['button_label']);
      break;

    case 'view':
      return array(
        'content' => drupal_get_form('createcontentblock_form'),
      );
  }
}

function createcontentblock_form() {
  $form = array();

  $subscribe = FALSE;
  if (module_exists('og')) {
    $group_node = og_get_group_context();
    global $user;
    if (isset($user->og_groups[$group_node->nid])) {
      // add only OG groups we have access to
      $group_nid = $group_node->nid;
      $form['gid'] = array(
        '#type' => 'hidden',
        '#value' => $group_nid,
      );
    }
    else {
      $subscribe = $group_node->nid;
    }
    $exempt = array_merge(variable_get('og_node_types', array('og')), variable_get('og_omitted', array()));
  }
  else {
    $exempt = array();
  }

  $options[] = t('-- Select One --');
  foreach (node_get_types() as $type) {
    if (module_invoke(node_get_types('module', $type), 'access', 'create', $type)) {
      $name = $type->name;
      if (isset($group_nid)) {
        if (!in_array($type->type, $exempt)) {
          $name .= ' (this group)';
        }
      }
      else {
        if (!in_array($type->type, $exempt)) {
          if (isset($subscribe)) {
            unset($name);
          }
          else {
            $name .= ' (for groups)';
          }
        }
      }
      if (isset($name)) {
        $options[$type->type] = $name;
      }
    }
  }
  if ($subscribe) {
    $form['subscribe'] = array(
      '#value' => l(t('Subscribe to Add Content to this Group'), 'og/subscribe/'. $subscribe, array(), 'destination=node/'. $subscribe),
    );
  }
  $form['type'] = array(
    '#type' => 'select',
    '#options' => $options,
    '#default_value' => 0,
    '#attributes' => array('onchange' => 'javascript:submit()'), // @TODO: implement in jquery
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => variable_get('createcontentblock_button_label', t('Create Content')),
  );
  $form['#submit'] = array('createcontentblock_form_submit' => array());
  return $form;
}

function createcontentblock_form_submit($form_id, &$form) {
  if (!empty($form['type'])) {
    if (empty($form['gid'])) {
      drupal_goto('node/add/'. $form['type']);
    }
    else {
      drupal_goto('node/add/'. $form['type'], 'gids[]='. $form['gid']);
    }
  }
  elseif (isset($form['gid'])) {
    drupal_goto('node/add-og', 'gids[]='. $form['gid']);
  } 
  else {
    drupal_goto('node/add');
  }
}

/**
 * Present a node submission form or a set of links to such forms.
 */
function createcontentblock_node_add_page() {
  if (!module_exists('og')) {
    drupal_goto('node/add');
    return;
  }

  global $user;

  if (is_numeric($_GET['gids'][0])) {
    if ($group_node = node_load($_GET['gids'][0])) {
      $group_name = theme('og_group_name', $group_node);
    }
  }

  $types = node_get_types();

  $exempt = array_merge(variable_get('og_node_types', array('og')), variable_get('og_omitted', array()));

  // display a node type overview.
  foreach ($types as $type) {
    if (function_exists($type->module .'_form') && node_access('create', $type->type)) {
      $type_url_str = str_replace('_', '-', $type->type);
      $title = t('Add a new @s.', array('@s' => $type->name));
      $link_title = drupal_ucfirst($type->name);
      if (isset($group_node) && !in_array($type->type, $exempt)) {
        $type_url_str .= "?gids[]=". $group_node->nid;
        $link_title .= ' - '. $group_name;
      }
      $out = '<dt>'. l($link_title, "node/add/$type_url_str", array('title' => $title)) .'</dt>';
      $out .= '<dd>'. filter_xss_admin($type->description) .'</dd>';
      $item[$type->type] = $out;
    }
  }

  if (isset($item)) {
    uksort($item, 'strnatcasecmp');
    $output = t('Choose the appropriate item from the list:') .'<dl>'. implode('', $item) .'</dl>';
  }
  else {
    $output = t('No content types available.');
  }

  return $output;
}

/**
 * Override this theme function to change the title of the OG group on the node/add-og page
 */
function theme_og_group_title($node) {
  return $node->title;
}
